import psycopg2
import tkinter as tk
from tkinter import messagebox

root = tk.Tk()  # Создаём объект

# Начало метода подключения
frame_main = tk.LabelFrame(root, text="Строка подключения")  # Создание контейнера,
# в которой будет лежать строка подключения

db_text = tk.StringVar()  # Задаём переменные
user_text = tk.StringVar()  # для сбора данных из элементов
pass_text = tk.StringVar()  # и вписывании их в строку подключения
host_text = tk.StringVar()  #
port_text = tk.StringVar()  #

constring = [  # Перечисление объектов для строки подключения
    # Название, переменная, значение по умолчанию
    ("БД", db_text, "python"),
    ("Пользователь", user_text, "postgres"),
    ("Пароль", pass_text, "5"),
    ("Путь к БД", host_text, "localhost"),
    ("Порт БД", port_text, "5432")
]

font_size = 36  # Переменная для установки шрифта

for i in constring:  # Цикл для создания строки подключения
    frame = tk.Frame(frame_main)  # контейнер
    frame.pack()

    i[1].set(i[2])  # В компонент записываем значение по умолчанию из коллекции

    lbl = tk.Label(frame, text=i[0], font=font_size)
    if i[1] != pass_text:  # Сокрытие пароля (Иван Жигунов)
        textbox_db = tk.Entry(frame, textvariable=i[1], font=font_size)
    else:
        textbox_db = tk.Entry(frame, show='*', textvariable=i[1], font=font_size)  # Отображение *

    lbl.pack(side="left")
    textbox_db.pack(side="left")

connection = None  # Глобальная переменная connection


def connection_postgre():
    global connection
    try:
        connection = psycopg2.connect(  # Подключение к БД PostgreSQL
            database=db_text.get(),  # Название БД
            user=user_text.get(),  # Имя пользователя
            password=pass_text.get(),  # Пароль пользователя
            host=host_text.get(),  # Путь БД (localhost) (127.0.0.1)
            port=port_text.get()  # Порт БД
        )
    except psycopg2.OperationalError as e:
        messagebox.showerror(f"Подключится к БД не удалось\n{e}")
    else:
        messagebox.showinfo("Вы успешно подключились к БД")
        btn_disconnect.config(state="normal")  # Изменение свойств кнопки
        btn_connect.config(state="disabled")  # Изменение свойств кнопки


def disconnect_postgre():
    global connection
    try:
        connection.close()
        btn_disconnect.config(state="disabled")  # Изменение свойств кнопки
        btn_connect.config(state="normal")  # Изменение свойств кнопки (Дарья)
        messagebox.showwarning(message="Вы отключились к БД")
    except Exception as e:
        messagebox.showerror(message=f"Что-то пошло не так\n{e}")


btn_connect = tk.Button(  # Кнопка подключения, с функцией connection_postgre()
    root,
    text="Подключение к БД",
    command=connection_postgre
)

btn_disconnect = tk.Button(  # Кнопка отключения, с функцией disconnect_postgre()
    root,
    text="Отключится от БД",
    command=disconnect_postgre,
    state="disabled"
)
frame_main.pack()
btn_connect.pack()
btn_disconnect.pack()
# Окончание метода подключения

# Начало метода запроса sql
frame_sql = tk.LabelFrame(root, text="SQL запрос")


def execute_sql_command():
    global connection
    try:
        cursor = connection.cursor()
        cursor.execute(sql_command.get())
        cursor.close()
        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


sql_command = tk.StringVar()

tk.Entry(frame_sql, font=font_size, width=50, textvariable=sql_command).pack()
tk.Button(frame_sql, font=font_size, text="Выполнить запрос", command=execute_sql_command).pack()
frame_sql.pack()
# Окончание метода запроса sql

# Начало метода добавления сотрудников
frame_insert = tk.LabelFrame(root, text="Добавление работника")

name_ins_text = tk.StringVar()
fname_ins_text = tk.StringVar()
salary_ins_text = tk.IntVar()

insstring = [
    ("Имя", name_ins_text),
    ("Фамилия", fname_ins_text),
    ("Оклад", salary_ins_text)
]

frame_ins_text = tk.Frame(frame_insert)

for name, var in insstring:
    frame = tk.Frame(frame_ins_text)
    frame.pack(side="left")

    tk.Label(frame, font=font_size, text=name).pack()
    tk.Entry(frame, font=font_size, textvariable=var).pack()

def insert_employee():
    global connection
    try:
        cursor = connection.cursor()
        cursor.execute("INSERT INTO employee (name,firstname,salary) VALUES (%s,%s,%s)",
                       (name_ins_text.get(),
                        fname_ins_text.get(),
                        salary_ins_text.get()
                        )
                       )
        cursor.close()
        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


tk.Button(frame_insert, font=font_size, text="Добавить сотрудника", command=insert_employee).pack()

frame_ins_text.pack()
frame_insert.pack()
# Окончание метода добавления сотрудников

# Начало метод удаления сотрудника

frame_delete = tk.LabelFrame(root, text="Удаление сотрудника")

listbox = tk.Listbox(frame_delete, selectmode="extended", width=30)

# BROWSE - выбор элемента с возможностю перетаскивания
# SINGLE - выбор элемента
# MULTIPLE - выбор нескольких различных элементов
# EXTENDED - выбор группы элементов

list = []


def get_employee():
    global connection, listbox, list
    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM employee")
        list = cursor.fetchall()
        listb = []
        for i in list:
            listb.append(i[1])
        emp_list = tk.Variable(value=listb)

        listbox.config(listvariable=emp_list)

        cursor.close()
        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


tk.Button(frame_delete, text="Получить сотрудников", command=get_employee).pack()


def delete_employee():
    global connection, list
    try:
        index = listbox.curselection()  # Получение индекса выбранного элемента (Кортеж)
        id = list[index[0]][0]

        # [
        # (1,Иван, Смирнов, 62000)
        # ...
        # ]
        cursor = connection.cursor()
        cursor.execute("DELETE FROM employee WHERE id = %s",
                       (id,)
                       )
        cursor.close()
        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
        get_employee()
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


tk.Button(frame_delete, text="Удалить сотрудника", command=delete_employee).pack()
listbox.pack()
frame_delete.pack()
# Окончание метода удаления сотрудников

from tkinter import ttk

frame_select = tk.LabelFrame(root, text="Вывод сотрудников")


def get_employee_select():
    global connection, list_select, treev

    try: # Удаление существующей таблицы
        treev.destroy()
    except:
        pass

    try:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM employee")
        list_select = cursor.fetchall()
        cursor.close()

        columns_employee = ("firstname", "name", "salary")
        treev = ttk.Treeview(frame_select, columns=columns_employee, show="headings")

        treev.heading("name", text="Имя")
        treev.heading("firstname", text="Фамилия")
        treev.heading("salary", text="Оклад")

        for employee in list_select:
            treev.insert("", "end", iid=employee[0], values=employee[1:])

        treev.pack()

        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


tk.Button(frame_select, text="Получить сотрудников", command=get_employee_select).pack()
frame_select.pack()
# Окончание метода вывода

# Начало метода изменения
frame_update = tk.LabelFrame(root, text="Изменение сотрудников")

name_upd_text = tk.StringVar()
fname_upd_text = tk.StringVar()
salary_upd_text = tk.IntVar()

updstring = [
    ("Фамилия", fname_upd_text),
    ("Имя", name_upd_text),
    ("Оклад", salary_upd_text)
]

frame_upd_text = tk.Frame(frame_update)

for name, var in updstring:
    frame = tk.Frame(frame_upd_text)
    frame.pack(side="left")

    tk.Label(frame, font=font_size, text=name).pack()
    tk.Entry(frame, font=font_size, textvariable=var).pack()

frame_upd_text.pack()


def select_upd_employee():
    index = treev.focus()
    employee = treev.item(index)["values"]
    name_upd_text.set(employee[0])
    fname_upd_text.set(employee[1])
    salary_upd_text.set(int(employee[2]))


def update_employee():
    global connection
    try:
        index = treev.focus()
        # employee = treev.item(index)["values"]
        # employee_child = treev.get_children(employee)

        cursor = connection.cursor()
        cursor.execute("UPDATE employee SET name=%s,firstname=%s,salary=%s WHERE id=%s",
                       (name_upd_text.get(),
                        fname_upd_text.get(),
                        salary_upd_text.get(),
                        index
                        )
                       )
        cursor.close()
        connection.commit()
        messagebox.showinfo(message="Команда была успешно выполнена")
    except Exception as e:
        connection.rollback()
        messagebox.showerror(message=f"Команда не было успешно выполнена\n{e}")


tk.Button(frame_update, text="Загрузить сотрудника", command=select_upd_employee).pack()
tk.Button(frame_update, text="Изменение сотрудников", command=update_employee).pack()

frame_update.pack()
# Окончание метода изменения


root.mainloop()

# Создание элементов напрямую
#
# frame = tk.Frame(frame_main)
# frame.pack()
#
# lbl = tk.Label(frame, text="Пользователь")
# textbox_db = tk.Entry(frame, textvariable=user_text)
# lbl.pack(side="left")
# textbox_db.pack(side="left")
#
# frame = tk.Frame(frame_main)
# frame.pack()
#
# lbl = tk.Label(frame, text="Пароль")
# textbox_db = tk.Entry(frame, textvariable=pass_text)
# lbl.pack(side="left")
# textbox_db.pack(side="left")
#
# frame = tk.Frame(frame_main)
# frame.pack()
#
# lbl = tk.Label(frame, text="Путь БД")
# textbox_db = tk.Entry(frame, textvariable=host_text)
# lbl.pack(side="left")
# textbox_db.pack(side="left")
#
# frame = tk.Frame(frame_main)
# frame.pack()
#
# lbl = tk.Label(frame, text="Порт")
# textbox_db = tk.Entry(frame, textvariable=port_text)
# lbl.pack(side="left")
# textbox_db.pack(side="left")
